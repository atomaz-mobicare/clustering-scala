package br.com.mobicare.clustering.mapper

import br.com.mobicare.clustering.model.GeoPoint
import br.com.mobicare.clustering.model.UserGeoPoint
import org.apache.spark.api.java.function.MapFunction
import org.apache.spark.sql.Row
import java.util

object ScUserGeopointMapper {
  final val terminals = new util.HashMap[String, util.List[GeoPoint]]
}

class ScUserGeopointMapper() extends MapFunction[Row, UserGeoPoint] {

  // private Long count = 0l;
  @throws[Exception]
  override def call(row: Row): UserGeoPoint = {

    var terminal = row.getString(0)
    val lat = row.getDouble(1)
    val lon = row.getDouble(2)

    if (terminal == null) terminal = "NULL"
    val u = new UserGeoPoint(terminal, lat, lon)
    if (!ScUserGeopointMapper.terminals.containsKey(u.getTerminal))
      ScUserGeopointMapper.terminals.put(u.getTerminal, new util.ArrayList[GeoPoint])
    ScUserGeopointMapper.terminals.get(u.getTerminal).add(new GeoPoint(lat, lon))

    u
  }

  def getTerminals: util.Map[String, util.List[GeoPoint]] = ScUserGeopointMapper.terminals
}