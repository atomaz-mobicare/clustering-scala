package br.com.mobicare.clustering.util;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.ml.distance.DistanceMeasure;
import org.apache.log4j.Logger;

public class HaversineDistance implements DistanceMeasure {

    private static final Logger LOGGER = Logger.getLogger(HaversineDistance.class);

    /**
     *
     * This uses the ‘haversine’ formula to calculate the great-circle distance between two points –
     * that is, the shortest distance over the earth’s surface – giving an ‘as-the-crow-flies’ distance
     * between the points (ignoring any hills they fly over, of course!).
     *
     * Haversine
     * formula:	a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
     * c = 2 ⋅ atan2( √a, √(1−a) )
     * d = R ⋅ c
     * where	φ is latitude, λ is longitude, R is earth’s radius (mean radius = 6,371km);
     * note that angles need to be in radians to pass to trig functions!
     *
     * Font: https://www.movable-type.co.uk/scripts/latlong.html?from=%2048.9642200,-122.0368100&to=48.965496,-122.072989
     * */
    @Override
    public double compute(double[] p1, double[] p2) throws DimensionMismatchException {

        double R = 6371e3; // metres
        double φ1 = Math.toRadians(p1[0]); // lat1.toRadians
        double φ2 = Math.toRadians(p2[0]); // lat2.toRadians
        double Δφ = Math.toRadians(p2[0]-p1[0]);
        //double Δφ = φ1 - φ2;
        double Δφby2 = Δφ/2;
        double λ1 = Math.toRadians(p1[1]);
        double λ2 = Math.toRadians(p2[1]);
        double Δλ = Math.toRadians(p2[1]-p1[1]);
        //double Δλ = λ1 -λ2;
        double Δλby2 = Δλ/2;

        double a = Math.sin(Δφby2) * Math.sin(Δφby2) + Math.cos(φ1) * Math.cos(φ2) *
                Math.sin(Δλby2) * Math.sin(Δλby2);
        //double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double c = 2 * Math.asin(Math.sqrt(a));

        //double d = R * c;
        //return d;

        return c;
    }

}
