package br.com.mobicare.clustering

import java.io.{File, FileNotFoundException, IOException}
import java.text.{ParseException, SimpleDateFormat}
import java.util.Map.Entry
import java.util.function.Consumer
import java.util.{Calendar, List}

import br.com.mobicare.clustering.mapper.ScUserGeopointMapper
import br.com.mobicare.clustering.model.{GeoPoint, UserGeoPoint}
import br.com.mobicare.clustering.service.{ClusterService, MailService}
import com.amazonaws.services.s3.{AmazonS3, AmazonS3ClientBuilder}
import com.amazonaws.services.s3.model.{GetObjectRequest, ListObjectsRequest, ObjectListing, S3ObjectSummary}
import org.apache.commons.io.FileUtils
import org.apache.hadoop.fs.LocalFileSystem
import org.apache.hadoop.hdfs.DistributedFileSystem
import org.apache.log4j.{Logger, Priority}
import org.apache.spark
import org.apache.spark.sql.{Dataset, Row, SparkSession}

import scala.util.control.Breaks._
import scala.collection.JavaConversions._


object Prop {
  var emails  : String = ""
  var date : String = ""
  var interval_days : String = ""
  var s3PrefixSources : Array[String] = null
  var s3PrefixTarget : String = ""

  var s3BucketNameSource = "oiwifi-athena-staging-tables"
  var s3BucketNameTarget = "oiwifi-athena-consolidation"

  //var s3BucketNameSource = "atomaz";
  //var s3BucketNameTarget = "atomaz";

  var clusterName = "user-location"

  //var parquetPathSource = "/home/ec2-user/s3-parquet/";
  //var parquetPathTarget = "/home/ec2-user/centroids/";

  var parquetPathSource = "/home/alicetomaz/Documents/git-clustering/temp/s3-parquet/"
  var parquetPathTarget = "/home/alicetomaz/Documents/git-clustering/temp/centroids/"

  var s3Client : AmazonS3 = null
}

object Application extends App {


  override def main(args: Array[String]) {

    val logger : Logger = Logger.getLogger(" :: CLUSTER :: ")

    try {
      logger.log(Priority.INFO, "Processando args ... ")
      readApplicationArguments(args)

      Prop.s3Client = AmazonS3ClientBuilder.defaultClient();
      logger.log(Priority.INFO, "Lendo parquet do S3 ")
      readParquetFromS3()

      logger.log(Priority.INFO, "Inicializando Spark")
      val spark : SparkSession = SparkSession.builder().appName("Clustering")
        .config("spark.master","local")
        .config("spark.local.dir","/dev/shm")
        .config("spark.network.timeout", "90000s")
        .config("spark.files.fetchTimeout", "600s")
        .config("spark.driver.cores", "64")
        .config("spark.driver.maxResultSize", "10g")
        .config("spark.driver.memory", "8g")
        .config("spark.executor.memory", "128g")

//        .config("spark.driver.cores", "4")
//        .config("spark.driver.memory", "8g")
//        .config("spark.executor.memory", "8g")

        .config("spark.executor.heartbeatInterval", "600s")
        .getOrCreate()
      spark.sparkContext.hadoopConfiguration.set("fs.hdfs.impl", classOf[DistributedFileSystem].getName )
      spark.sparkContext.hadoopConfiguration.set("fs.file.impl", classOf[LocalFileSystem].getName )



      printf("Lendo arquivo parquet\n")
      logger.log(Priority.INFO, "Lendo arquivo parquet")
      val parquetFile = loadParquetFile(spark)
      parquetFile.createOrReplaceTempView("UserLatLon")

      printf("Extraindo dados do parquet via SQL\n")
      logger.log(Priority.INFO, "Extraindo dados do parquet via SQL")
      val results = parquetFile.sqlContext.sql("SELECT distinct terminal, lat, long FROM UserLatLon")
      val mapper = new ScUserGeopointMapper()

      printf("Agrupando resultados por terminal\n")
      logger.log(Priority.INFO, "Agrupando " + results.count() + " resultados por terminal")
      results.foreach(row => mapper.call(row))
      //val rows : List[Row] = results.collectAsList()

      printf("Calculando distancia\n")
      val clusterApplication = new ClusterApplication()
      val terminals = ScUserGeopointMapper.terminals

      printf("Calculando distancia\n")
      logger.log(Priority.INFO, "Calculando distancia para " + terminals.size() + " terminais")
      val centroids : List[UserGeoPoint]= clusterApplication.run(terminals)

      val targetPathFile : File = new File(Prop.parquetPathTarget);
      if (targetPathFile.exists)
        FileUtils.deleteDirectory(targetPathFile)

      logger.log(Priority.INFO, "Exportando para parquet " + centroids.size() + " resultados")
      spark.sqlContext.sparkSession
        .createDataFrame(centroids, classOf[UserGeoPoint])
        .withColumnRenamed("lon", "long")
        .write.parquet(Prop.parquetPathTarget)

      logger.log(Priority.INFO, "Exportando parquet para o S3")
      writeParquetToS3()

    } catch {
      case e: Exception => {
        e.printStackTrace()
        val mailService : MailService = new MailService();
        mailService.sendEmail(Prop.clusterName, Prop.date, e.getMessage(), Prop.emails);
      }
    }
  }

  def loadParquetFile(spark : SparkSession): Dataset[Row] = {
    val parquetFile : Dataset[Row] = spark.read.parquet(Prop.parquetPathSource)
    parquetFile.createOrReplaceTempView("UserLatLon")
    parquetFile.sqlContext.sql("SELECT count(*) from UserLatLon").show()

    parquetFile
  }

  @throws[IOException]
  @throws[ParseException]
  private def readApplicationArguments(args: Array[String]): Unit = {
    for (arg <- args) {
      val keyValue = arg.split("=")
      val key = keyValue(0)
      if ("--emails" == key) Prop.emails = keyValue(1)
      else if ("--dt" == key) Prop.date = keyValue(1)
      else if ("--interval-days" == key) Prop.interval_days = keyValue(1)
    }
    if (Prop.interval_days == null || Prop.interval_days.isEmpty) Prop.interval_days = "1"
    val formatter = new SimpleDateFormat("yyyy-MM-dd")
    if (Prop.date == null || Prop.date.isEmpty) {
      val calendar = Calendar.getInstance
      calendar.add(Calendar.DAY_OF_MONTH, -1)
      val yesterday = calendar.getTime
      Prop.date = formatter.format(yesterday)
    }
    Prop.s3PrefixSources = new Array[String](new Integer(Prop.interval_days))
    Prop.s3PrefixTarget = Prop.clusterName + "-cluster" + "/dt=" + Prop.date + "/"
    var currentDate = Prop.date
    var i = 0
    while ( i < Prop.s3PrefixSources.length ) {
      Prop.s3PrefixSources(i) = Prop.clusterName + "/" + currentDate + "/"
      println(Prop.s3PrefixSources(i))
      currentDate = getDate(Prop.date, (i + 1) * -1)
      i += 1;
    }
  }

  @throws[ParseException]
  private def getDate(current: String, minus: Int) = {
    val formatter = new SimpleDateFormat("yyyy-MM-dd")
    val date = formatter.parse(current)
    val calendar = Calendar.getInstance
    calendar.setTime(date)
    calendar.add(Calendar.DAY_OF_MONTH, minus)
    formatter.format(calendar.getTime)
  }

  private def toJavaConsumer[T](consumer: (T) => Unit): Consumer[T] ={
    new Consumer[T] {
      override def accept(t: T): Unit = {
        consumer(t)
      }
    }
  }

  @throws[IOException]
  private def readParquetFromS3(): Unit = {
    val dir: File = new File(Prop.parquetPathSource)
    if (!dir.exists) dir.mkdir

    Prop.s3PrefixSources.foreach(s3PrefixSource => {
      val s3dir: String = "s3://" + Prop.s3BucketNameSource + "/" + s3PrefixSource
      println("Lendo arquivo Parquet = " + s3dir)
      val listObjectsRequest: ListObjectsRequest = new ListObjectsRequest().withBucketName(Prop.s3BucketNameSource).withPrefix(s3PrefixSource)
      val objects: ObjectListing = Prop.s3Client.listObjects(listObjectsRequest)
      val summaries: List[S3ObjectSummary] = objects.getObjectSummaries
      printf("Número de arquivos encontrados em [%s] = %d\n" , s3dir, summaries.size)
      if (summaries.size < 1) throw new FileNotFoundException("Diretório [" + s3dir + "] está vazio.")
      var key: String = null

      breakable {
        for (summary <- summaries) {
          if (summary.getKey.endsWith(".parquet")) {
            key = summary.getKey
            break
          }
        }
      }
      if (key == null) throw new FileNotFoundException("Não foi encontrado um parquet em " + s3dir)
      val filename: String = Calendar.getInstance.getTimeInMillis + ".parquet"
      val file: File = new File(Prop.parquetPathSource + filename)
      file.createNewFile
      Prop.s3Client.getObject(new GetObjectRequest(Prop.s3BucketNameSource, key), file)
    })

  }


  @throws[IOException]
  private def writeParquetToS3(): Unit = {
    val centroidParquetDir: File = new File(Prop.parquetPathTarget)
    if (centroidParquetDir.isDirectory) {

      val files: Array[File] = centroidParquetDir.listFiles().filter(file => file.getName().endsWith(".parquet"))
      if (files.length == 0) throw new FileNotFoundException("Unable to find parquet file at '" + Prop.parquetPathTarget + "'")
      for (f <- files) {
        Prop.s3Client.putObject(Prop.s3BucketNameTarget, Prop.s3PrefixTarget + f.getName, f)
      }
    }
  }

}
