package br.com.mobicare.clustering.mapper;

import br.com.mobicare.clustering.model.GeoPoint;
import br.com.mobicare.clustering.model.UserGeoPoint;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Row;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserGeopointMapper implements MapFunction<Row, UserGeoPoint> {

    private final Map<String, List<GeoPoint>> terminals = new HashMap<>();

    // private Long count = 0l;
    @Override
    public UserGeoPoint call(Row row) {
        //System.out.println("Row " + (++count));
        String terminal = row.getString(0);
        Double lat = row.getDouble(1);
        Double lon = row.getDouble(2);
        //Double lat = Double.parseDouble(row.getString(1));
        //Double lon = Double.parseDouble(row.getString(2));
        if (terminal == null)
            terminal = "NULL";

        UserGeoPoint u = new UserGeoPoint(terminal, lat, lon);

        if (!terminals.containsKey(u.getTerminal())) {
            terminals.put(u.getTerminal(), new ArrayList<>());
        }
        terminals.get(u.getTerminal()).add(new GeoPoint(lat,lon));

        return u;
    }

    public Map<String, List<GeoPoint>> getTerminals() {
        return terminals;
    }
}
