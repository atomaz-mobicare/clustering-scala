package br.com.mobicare.clustering.model;

import org.apache.commons.math3.ml.clustering.Clusterable;

import java.io.Serializable;

public class GeoPoint implements Serializable, Clusterable {

    private Double lat;
    private Double lon;

    public GeoPoint() {}
    public GeoPoint(Double lat, Double lon) {
        this.lat= lat;
        this.lon= lon;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return lat + "," + lon;
    }

    public double[] getPoint() {
        return new double[] { lat, lon };
    }

}
