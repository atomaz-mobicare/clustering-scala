package br.com.mobicare.clustering.model;

import javax.annotation.Nullable;
import java.io.Serializable;

public class UserGeoPoint implements Serializable {

    @Nullable
    private String terminal;

    @Nullable
    private Double lat;

    @Nullable
    private Double lon;

    @Nullable
    private Integer size;

    public UserGeoPoint() {}
    public UserGeoPoint(String terminal, Double lat, Double lon) {
        this.terminal = terminal;
        this.lat = lat;
        this.lon = lon;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Override
    public String toString() {
        String result = terminal + ","+ lat + "," + lon;

        return result;
    }
}
