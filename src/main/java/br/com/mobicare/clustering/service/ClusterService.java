package br.com.mobicare.clustering.service;

import br.com.mobicare.clustering.model.GeoPoint;
import br.com.mobicare.clustering.util.HaversineDistance;
import br.com.mobicare.clustering.model.UserGeoPoint;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ClusterService implements Consumer<Map.Entry<String, List<GeoPoint>>> {

    private final List<UserGeoPoint> centroids = new LinkedList<>();
    private final List<Row> rows = new LinkedList<>();
    private static final HaversineDistance distance = new HaversineDistance();
    private static final DBSCANClusterer<GeoPoint> dbscanClusterer = new DBSCANClusterer<>(0.000005, 0, distance);
    private static Schema schema;
    List<GenericData.Record> centroidList = new LinkedList<>();

    public ClusterService(Schema schemaAvro) {
        this.schema = schemaAvro;
    }

    public ClusterService() {
    }

    @Override
    public void accept(Map.Entry<String, List<GeoPoint>> terminalEntry) {
        List<Cluster<GeoPoint>> clusters = dbscanClusterer.cluster(terminalEntry.getValue());

        List<UserGeoPoint> centroids = clusters.parallelStream()
                .map(new CentroidService(terminalEntry.getKey()))
                .collect(Collectors.toList());

        this.centroids.addAll(centroids);
    }

    public List<UserGeoPoint> getCentroids() {
        centroids.removeIf(Objects::isNull);
        return centroids;
    }
    public List<GenericData.Record> getCentroidList() {
        centroidList.removeIf(Objects::isNull);
        return centroidList;
    }

    public List<Row> getRows() {
        rows.removeIf(Objects::isNull);
        return rows;
    }

}
