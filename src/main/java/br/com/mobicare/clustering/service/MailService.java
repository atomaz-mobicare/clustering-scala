package br.com.mobicare.clustering.service;


import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

public class MailService {

    private String SUBJECT = "[ERRO-CLUSTERING] ";
    private String FROM_ADDR = "equipe-bd@mobicare.com.br";
    private String SMTP_SERVER = "email-smtp.us-east-1.amazonaws.com";
    private String SMTP_USERNAME = "AKIAJHT5RQDXPYB25XBQ";
    private String SMTP_PASSWORD = "AlhV3q7GBHzZvHslBNUiANt+bl77SZIjGa6ncblX41N0";
    private String SMTP_PORT = "587";

    /**
     * Utility method to send simple HTML email
     * @param message
     * @param emails
     */
    public void sendEmail(String clusterName, String dt, String message, String emails){

        String[] mails = emails.split(",");
        try
        {
            for (String email: mails) {
                MimeMessage msg = new MimeMessage(getSession());
                //set message headers
                msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
                msg.addHeader("format", "flowed");
                msg.addHeader("Content-Transfer-Encoding", "8bit");

                msg.setFrom(new InternetAddress(FROM_ADDR, "NoReply-Clustering-Service"));

                String sub = SUBJECT + clusterName + "/" + dt;
                msg.setSubject(sub, "UTF-8");
                msg.setText(message, "UTF-8");
                msg.setSentDate(new Date());

                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
                System.out.println("Message is ready");
                Transport.send(msg);

                System.out.println("E-mail Sent Successfully!!");
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Session getSession() {
        Properties props = System.getProperties();
        props.put("mail.smtp.host", SMTP_SERVER);
        props.put("mail.smtp.port", SMTP_PORT);
        props.put("mail.smtp.auth", true);

        return Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        SMTP_USERNAME, SMTP_PASSWORD);
            }
        });
    }

}
