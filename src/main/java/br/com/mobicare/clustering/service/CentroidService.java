package br.com.mobicare.clustering.service;

import br.com.mobicare.clustering.Application;
import br.com.mobicare.clustering.model.GeoPoint;
import br.com.mobicare.clustering.util.HaversineDistance;
import br.com.mobicare.clustering.model.UserGeoPoint;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.log4j.Logger;

import java.util.function.Function;

public class CentroidService implements Function<Cluster<GeoPoint>, UserGeoPoint> {
    private static Logger LOGGER = Logger.getLogger(CentroidService.class.getSimpleName());
    private String terminal;

    public CentroidService(String terminal) {
        this.terminal = terminal;
    }
    @Override
    public UserGeoPoint apply(Cluster<GeoPoint> cluster) {
        double x=0,y=0,z=0;
        double radianLat, radianLon;
        double mx, my, mz;
        double centerLon , hyp, centerLat;
        double hdistance, lowestDistance;
        GeoPoint centroid, finalCentroid;
        UserGeoPoint userCentroid;
        HaversineDistance haversineDistance = new HaversineDistance();

        for(GeoPoint p : cluster.getPoints()) {
            radianLat = Math.toRadians(p.getLat());
            radianLon = Math.toRadians(p.getLon());
            x += Math.cos(radianLat) * Math.cos(radianLon);
            y += Math.cos(radianLat) * Math.sin(radianLon);
            z += Math.sin(radianLat);
        }

        mx = x / cluster.getPoints().size();
        my = y / cluster.getPoints().size();
        mz = z / cluster.getPoints().size();
        centerLon = Math.atan2(my, mx);
        hyp = Math.sqrt(mx*mx + my*my);
        centerLat = Math.atan2(mz, hyp);
        centroid = new GeoPoint();
        centroid.setLat(centerLat);
        centroid.setLon(centerLon);

        lowestDistance = Double.MAX_VALUE;
        finalCentroid = null;

        for(GeoPoint p : cluster.getPoints()) {
            hdistance = haversineDistance.compute(centroid.getPoint(), p.getPoint());
            if (hdistance < lowestDistance) {
                lowestDistance = hdistance;
                finalCentroid = p;
            }
        }

        if (finalCentroid.getLat() == null || finalCentroid.getLon() == null) {
            LOGGER.info("=========== DADOS NULOS Terminal= " +terminal+ " = " + finalCentroid);
            return null;
        }

        userCentroid = new UserGeoPoint();
        userCentroid.setTerminal(terminal);
        userCentroid.setLat(finalCentroid.getLat());
        userCentroid.setLon(finalCentroid.getLon());
        userCentroid.setSize(cluster.getPoints().size());
        //System.out.println("Terminal= " +terminal+ ", cluster_size=" + cluster.getPoints().size() + ", centroid=" + finalCentroid);
        return userCentroid;

    }
}
