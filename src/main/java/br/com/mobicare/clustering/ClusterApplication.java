package br.com.mobicare.clustering;

import br.com.mobicare.clustering.mapper.ScUserGeopointMapper;
import br.com.mobicare.clustering.mapper.UserGeopointMapper;
import br.com.mobicare.clustering.model.UserGeoPoint;
import br.com.mobicare.clustering.model.GeoPoint;
import br.com.mobicare.clustering.service.ClusterService;
import org.apache.spark.sql.Row;

import java.util.List;
import java.util.Map;

public class ClusterApplication {


    public Map<String, List<GeoPoint>> prepareCluster(List<Row> rows) throws Exception {

        final UserGeopointMapper mapper = new UserGeopointMapper();
        rows.parallelStream().forEach(mapper::call);

        return mapper.getTerminals();
    }


    public List<UserGeoPoint> run(Map<String, List<GeoPoint>> terminals) {

        System.out.println("Calculando distancia");
        final ClusterService consumer = new ClusterService();

        terminals.entrySet().parallelStream().forEach(consumer::accept);

        return consumer.getCentroids();


    }
}
